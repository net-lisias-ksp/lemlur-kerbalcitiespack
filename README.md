# Kerbal Cities Pack : Unofficial

This is an unofficial, non authorised repository for Kerbal Cities Pack for historical reference and troubleshooting.

You are authorised to fork this repository under Bitbucket [ToS](https://www.atlassian.com/legal/product-specific-terms#bitbucket-cloud-specific-terms). Any other right must be explicitly granted by additional licensing.


## In a Hurry

* [Binaries](https://bitbucket.org/net-lisias-ksp/lemlur-kerbalcitiespack/src/Archive/)
* [History](https://bitbucket.org/net-lisias-ksp/lemlur-kerbalcitiespack/src/History/)
* [Front Page](https://bitbucket.org/net-lisias-ksp/lemlur-kerbalcitiespack/src/master/)
* [Change Log](./CHANGE_LOG.md)


## References

* [lemlur](https://www.spacedock.info/profile/lemlur): Maintainer
	* [Spacedock](https://www.spacedock.info/mod/839/Kerbal%20Cities%20Pack)

